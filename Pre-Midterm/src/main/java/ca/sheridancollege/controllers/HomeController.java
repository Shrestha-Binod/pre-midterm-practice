package ca.sheridancollege.controllers;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;

import ca.sheridancollege.beans.Book;
import ca.sheridancollege.beans.Store;
import ca.sheridancollege.repositories.BookRepository;
import ca.sheridancollege.repositories.StoreRepository;

@Controller
public class HomeController {

	@Autowired
	private BookRepository bookRepos;
	@Autowired
	private StoreRepository storeRepos;

	@GetMapping("/")
	public String goHome(Model model) {

		model.addAttribute("Book", new Book());
		return "Home.html";
	}

	@GetMapping("/saveBook")
	public String saveBooks(@ModelAttribute Book book, Model model) {
		bookRepos.save(book);
		model.addAttribute("myBooks", bookRepos.findAll());
		model.addAttribute("Book", new Book());
		return "Home.html";
	}

	@GetMapping("/goAddStore")
	public String goAddStore(Model model) {
		model.addAttribute("Store", new Store());
		return "Store.html";
	}

	@GetMapping("/saveStore")
	public String saveStore(@ModelAttribute Store store, Model model) {
		storeRepos.save(store);
		model.addAttribute("myStores", storeRepos.findAll());
		model.addAttribute("Store", new Store());
		return "Store.html";
	}
	
	
	@GetMapping("/goPickBooks")
	public String pickBooks(Model model) {

		model.addAttribute("books", bookRepos.findAll());
		model.addAttribute("stores", storeRepos.findAll());

		return "ViewBooksAndStores.html";
	}
	
	@GetMapping("//addBookToStore")
	public String assignCourses(@RequestParam int bookid, @RequestParam int storeid) {

		// retriving data from database
		Book books = bookRepos.findById(bookid);
		Store stores = storeRepos.findById(storeid);
		books.getStores().add(stores);
		stores.getBooks().add(books);
		storeRepos.save(stores);		

		return "redirect:/goPickBooks";
	}

}
