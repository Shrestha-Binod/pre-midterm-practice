package ca.sheridancollege;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PreMidtermApplication {

	public static void main(String[] args) {
		SpringApplication.run(PreMidtermApplication.class, args);
	}

}
