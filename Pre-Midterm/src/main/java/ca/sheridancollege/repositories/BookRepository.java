package ca.sheridancollege.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import ca.sheridancollege.beans.Book;

public interface BookRepository extends CrudRepository<Book, Integer> {
	Book findById(int id);
	

}
