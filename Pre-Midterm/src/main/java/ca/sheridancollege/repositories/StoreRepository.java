package ca.sheridancollege.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import ca.sheridancollege.beans.Store;

public interface StoreRepository extends CrudRepository<Store, Integer> {
	Store findById(int id);

}
