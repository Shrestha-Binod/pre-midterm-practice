package ca.sheridancollege.beans;

import java.util.*;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class Book {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	private String title;
	private String author;
	private String price;
	private String quantity;
	private String course; // this var must match in th:field in home page.
	
	@Transient
	private String[] courses = {"Java", "C#", "Android","iOS", "DBMS"}; // this var must be passed in th:each
	
	@ManyToMany(mappedBy="books")
	private List<Store> stores = new ArrayList<Store>() ;

	public Book(Integer id, String title, String author, String price, String quantity) {
		super();
		this.id = id;
		this.title = title;
		this.author = author;
		this.price = price;
		this.quantity = quantity;
		
	}
	
	

}
